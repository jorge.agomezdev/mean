import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { Employee } from '../../interfaces/employee.interface';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  constructor(private employeeService: EmployeesService) {}

  employees: Employee[] = [];

  newEmployee: Employee = {
    name: '',
    position: '',
    office: '',
    salary: 0,
  };

  ngOnInit(): void {
    this.getEmployees();
  }

  resetForm(form: NgForm){
    form.reset();
  }


  getEmployees() {
    this.employeeService.getEmployees().subscribe((res) => {
      console.log('Información', res);
      this.employees = res;
      console.log(
        'Información ya procesada y lista para mostrar en html',
        this.employees
      );
    });
  }

  addEmployee(form: NgForm) {
    if(form.value._id){
      this.employeeService.putEmployee(form.value).subscribe(res=>{
        console.log(res);
        this.getEmployees();
        form.reset();
      },
      err=>{
        console.log(err);
      });
    }else{
      console.log(form.value);
    this.employeeService.createEmployee(form.value).subscribe(
      (res) => {
        console.log(res);
        this.getEmployees(); /* hacer solo cuando el formulario y la vista están en la misma página */
        form.reset(); /* para limpiar el formulario despues de guardar la información */
      },
      (err) => {
        console.log(err);
      }
    );
    }
  }

  deleteEmployee(id: string) {
    if (confirm('Are you sure you want to delete it?')) {
      this.employeeService.deleteEmployee(id).subscribe(
        (res) => {
          console.log(res);
          this.getEmployees(); /* hacer solo cuando el formulario y la vista estén en la misma página */
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
    }
  }

  putEmployee(employee: Employee){
    this.newEmployee = employee;
  }
}
