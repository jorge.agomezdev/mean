const express = require ('express');
const morgan = require ('morgan');
const cors = require ('cors');
/*const routes = require ('./routes/employees.routes');*/

const app = express();

/*establecemos una variable global para definir el puerto*/
app.set('port', process.env.PORT || 3500);


app.use(cors());
app.use(morgan('dev'));
/* para que pueda entender información json */
app.use(express.json())
/* para que pueda interpretar la información html de formularios */
app.use(express.urlencoded({extended: false}));

/* llamar las rutas desde el archivo de origen y establecer /employees como ruta principal para no sobre escribirla en las rutas*/
app.use("/employees", require('./routes/employees.routes'));

/*exportamos el modulo app para poder llamarlo desde otro archivo*/ 
module.exports = app;