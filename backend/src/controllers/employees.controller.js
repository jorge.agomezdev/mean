const employeesCtrl = {}
/* requerimos en una constante el modelo del esquema para guardar la información */
const employeeModel = require('../models/employee.model');

/* Metodo para crear al empleado nuevo */
employeesCtrl.createEmployee = async (req, res) => {
    
    const newEmployee = new employeeModel(req.body)
    console.log(newEmployee);

    await newEmployee.save();

    res.send({message: 'Employee Created'})
    
};

/* metodo para mostrar a todos los empleados */
employeesCtrl.getEmployees = async (req, res) => {
    const employees = await employeeModel.find()
    res.send(employees);
};


/* metodo para mostrar a empleado por id */
employeesCtrl.getEmployee = async (req, res) => {
    const employee = await employeeModel.findById(req.params.id)
    res.send(employee);
};


/* metodo para actualizar a empleado por su ID */
employeesCtrl.editEmployee = async (req, res) => {
    await employeeModel.findByIdAndUpdate(req.params.id, req.body)
    res.json({status: 'Empleado actualizado exitosamente'});
};


/* metodo para eliminar a empleado por su ID */
employeesCtrl.deleteEmployee = async (req, res) => {
    await employeeModel.findByIdAndDelete(req.params.id)
    res.json({status: 'Empleado Eliminado'})
};



module.exports =  employeesCtrl;