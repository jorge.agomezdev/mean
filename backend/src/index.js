/* requerimos el archivo de la base de datos*/
require('./database');

/* requerimos el modulo app desde el archivo app.js para usar nuestra variable global que contiene el puerto */
const app = require("./app");


app.listen(app.get('port'));

console.log('server on port', app.get('port'));