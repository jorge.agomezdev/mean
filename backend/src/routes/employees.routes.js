/* requerimos el modulo Router de express */
const { Router } = require ('express'); 
/* inicializamos el modulo roueter */
const router = Router();
/* requerimos las funciones de rutas desde el controller employees.controller */
const employeesCtrl = require ('../controllers/employees.controller');

/* Rutas de mi aplicación */
/* Create - Read - Update - Delete (CRUD) */

router.post('/', employeesCtrl.createEmployee);

router.get('/', employeesCtrl.getEmployees);

router.get('/:id', employeesCtrl.getEmployee);

router.put('/:id', employeesCtrl.editEmployee);

router.delete('/:id', employeesCtrl.deleteEmployee);




/* exportamos el modulo router */
module.exports = router;

